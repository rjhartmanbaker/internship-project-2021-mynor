# Internship Project 2021

The purpose of this project is to test the performance of a set of benchmark 
codes on the Perlmutter supercomputer that have been compiled in different ways 
(using different compilers and different options). We would like to be able to 
know what compiler to recommend for which purpose. To do this, you will learn 
first how to use a supercomputer, then run some benchmarks on Perlmutter and 
analyze the results. 

## Procedure

I know that there is a lot you don't know yet. That is expected and welcomed 
-- how else would you learn to do something new?

We will hold daily stand-up meetings. During this 30 minutes, we will go over 
your progress. I will teach you new skills and you can ask me any questions you 
have. You can also message me on Slack if you are stuck on something. I 
encourage you to try to learn something yourself before asking for help, because 
it will make you understand it better if you do that work first. Of course I do 
not expect you to be able to understand everything the first time!

By the end, you will have accomplished a very nice project that provides NERSC 
with valuable information on the performance of the compilers on various pieces 
of code. You will be able to present your work at the Computing Sciences Summer 
Student virtual poster session, and at school as well!

We will use this Gitlab repository to store and record your work all in one 
place. You will get edit access to this repository and add documentation as 
appropriate. I will also amend the documentation in this repository as we learn 
more about what doing these benchmarking runs entails.

## Accounts

First, you will need to get a NERSC account. On [Iris](https://iris.nersc.gov),
request an account, and select the `nintern` project as your project.

You also need a [Gitlab](https://gitlab.com) account. You can set this up by 
using your Lab email or your personal email.

## Initial Computer Skills

You will need to learn about the following: 
- Linux/Unix command-line commands
    - [Software Carpentry Shell Lesson](https://swcarpentry.github.io/shell-novice/)
    - Unix Tips file in this repository
- The vi editor
    - [Basic vi Commands](https://www.cs.colostate.edu/helpdocs/vi.html)
    - vi Tips file in this repository
- Gitlab and git command-line commands
    - [Software Carpentry Git Lesson](http://swcarpentry.github.io/git-novice/)
- Compiling codes
    - An application begins as source code files (on HPC systems, generally 
written in Fortran, C, or C++), which are then compiled into object files 
(generally suffixed with `.o` or `.a`), and then linked together into an 
executable. These steps are performed by a utility called a compiler, which 
parses the source files and translates them into machine-readable files (object 
files). This can be done by hand, but it becomes complicated and difficult when 
there are many files to be compiled. In this case, we use an automation tool 
such as Make or CMake.
    - [Software Carpentry Make Lesson](http://swcarpentry.github.io/make-novice/)

## Learning Supercomputer Concepts and Skills

- Read this article on
[Supercomputers](https://www.explainthatstuff.com/how-supercomputers-work.html)
for an overview of how supercomputers work.
- Read the [Getting Started](https://docs.nersc.gov/getting-started/) page on 
the NERSC documentation website. Watch the videos from the new user training.

## First Hands-On Tasks

- Run a test job on the Cori supercomputer. The "Basic MPI Script" on the
[Example Job Scripts](https://docs.nersc.gov/jobs/examples/) page can be
copied into a file (use the vi editor to edit your file!) and run.
- Take a look at the 
[NERSC Proxies](https://gitlab.com/NERSC/nersc-proxies/info) collection on 
Gitlab. Clone the `su3_bench` repository into your home directory on
Cori or Perlmutter. (The home file system is available on all NERSC resources 
so it will show up when you log on to any system). Read the README file and try
to understand what it is saying. If you don't, no problem, we can discuss in our
daily meeting.

## Starting the Project

- Compile the benchmark for the 
[Cori GPU nodes](https://docs-dev.nersc.gov/cgpu/), 
using the default programming environment, and run it on one node. Note what 
data it outputs.
- Recompile the benchmark for Perlmutter using the default programming
environment. Run the benchmark on Perlmutter.

12345678901234567890123456789012345678901234567890123456789012345678901234567890



